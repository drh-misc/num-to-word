package com.drh.misc.numtoword;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class NumberSection {
  private final String numStr;
  private final int numInt;

  public int asInt() {
    return numInt;
  }

  public String asString() {
    return numStr;
  }
}
