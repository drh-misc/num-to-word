package com.drh.misc.numtoword;

import java.util.ArrayList;
import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class NumberToWord {

  private static final String[] DIGITS = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
  private static final String[] TENS = {"ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
  private static final String[] TEENS = {"eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
  private static final String[] AMOUNTS = {"thousand", "million", "billion", "trillion", "quadrillion", "quintillion", "sextillion", "septillion", "octillion", "nonillion", "decillion"};

  private static final String COMMA_SEPARATOR = ", ";
  private static final String AND_SEPARATOR = " and ";

  private static final String HUNDRED = " hundred";
  private static final String ZERO = "zero";

  private static final String NUMBER_REGEX = "^(\\d+|\\d{1,3}(,\\d{3})+)$";

  public static String toWord(String number) {
    return parseNumber(validateAndSanitize(number));
  }

  public static String toWord(int number) {
    return parseNumber(String.valueOf(number));
  }

  public static String toWord(long number) {
    return parseNumber(String.valueOf(number));
  }

  private static String parseNumber(String number) {
    int amount = -1;
    List<NumberSection> numberSections = new ArrayList<>();
    int[] hundredsArray = getHundredsArray(number);

    for (int i = hundredsArray.length - 1; i >= 0; i--) {
      int hundred = hundredsArray[i];
      if (hundred > 0) {
        String section = calcHundred(hundred, amount);
        numberSections.add(0, new NumberSection(section, hundred));
      }
      amount++;
    }

    if (numberSections.isEmpty()) {
      return ZERO;
    }

    return join(numberSections);
  }

  private static int[] getHundredsArray(String number) {
    int noOfHundreds = getNumberOfHundreds(number);
    if (noOfHundreds == 1) {
      return new int[] { Integer.parseInt(number) };
    }

    int[] hundreds = new int[noOfHundreds];
    int start = number.length() - 3;
    for (int i = noOfHundreds - 1; i >= 0; i--) {
      if (start < 0) {
        hundreds[i] = Integer.parseInt(number.substring(0, start + 3));
      } else {
        hundreds[i] = Integer.parseInt(number.substring(start, start + 3));
      }
      start -= 3;
    }

    return hundreds;
  }

  private static int getNumberOfHundreds(String number) {
    int length = number.length();
    if (length % 3 > 0) {
      return length / 3 + 1;
    }
    return length / 3;
  }

  private static String calcHundred(int num, int amount) {
    int hundreds = num / 100;
    int tens = (num - hundreds * 100) / 10;
    int digits = num % 10;

    String hundredStr = null;
    String tensStr = null;
    String digitsStr = null;

    if (hundreds > 0) {
      hundredStr = DIGITS[hundreds - 1];
    }

    if (tens == 1 && digits > 0) {
      tensStr = TEENS[digits - 1];
    } else {
      if (tens > 0) {
        tensStr = TENS[tens - 1];
      }
      if (digits > 0) {
        digitsStr = DIGITS[digits - 1];
      }
    }

    return humanReadable(hundredStr, tensStr, digitsStr, amount);
  }

  private static String humanReadable(String hundreds, String tens, String digits, int amount) {
    String word = "";
    if (hundreds != null) {
      word += hundreds + HUNDRED;
    }

    if (tens != null || digits != null) {
      if (hundreds != null) {
        word += AND_SEPARATOR;
      }
    }

    if (tens != null) {
      word += tens;
    }

    if (digits != null) {
      if (tens != null) {
        word += '-';
      }
      word += digits;
    }

    String amountStr = getAmount(amount);
    if (amountStr != null) {
      word += ' ' + amountStr;
    }

    return word;
  }

  private static String join(List<NumberSection> wordSections) {
    int sections = wordSections.size();
    NumberSection lastSection = wordSections.get(sections - 1);

    if (sections == 1) {
      return lastSection.asString();
    }

    StringBuilder sb = new StringBuilder(wordSections.get(0).asString());
    for (int i = 1; i < sections - 1; i++) {
      sb.append(COMMA_SEPARATOR)
        .append(wordSections.get(i).asString());
    }

    sb.append((lastSection.asInt() < 100) ? AND_SEPARATOR : COMMA_SEPARATOR)
      .append(lastSection.asString());
    return sb.toString();
  }

  private static String getAmount(int amount) {
    return amount < 0 ? null : AMOUNTS[amount];
  }

  private static String validateAndSanitize(String number) {
    if (!number.matches(NUMBER_REGEX)) {
      throw new NumberFormatException("Invalid format provided: '" + number + '\'');
    }

    char[] chars = number.toCharArray();
    StringBuilder sb = new StringBuilder();
    boolean leadingZeros = true;
    for (char c : chars) {
      if (c == ',') continue;
      if (c == '0' && leadingZeros) continue;

      sb.append(c);
      leadingZeros = false;
    }
    return sb.toString();
  }
}
