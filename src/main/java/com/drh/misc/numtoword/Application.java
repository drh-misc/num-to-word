package com.drh.misc.numtoword;

import java.util.Random;

public class Application {

  public static void main(String[] args) throws InterruptedException {
    Random random = new Random();
    while (true) {
      long i = random.nextLong();
      if (i < 0) continue;
      System.out.printf("%3d : %s\n", i, NumberToWord.toWord(i));
      Thread.sleep(500);
    }

  }
}
